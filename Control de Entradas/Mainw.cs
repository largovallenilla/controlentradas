﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace Control_de_Entradas
{
    public partial class Mainw : Form
    {
        private SQLiteConnection mainw_Conn, mainw_db;
        private SQLiteDataAdapter mainw_Adapter;
        private DataTable mainw_DTable;
        private SQLiteDataReader dr;

        private string selectedDbPath, selectedDbType, selectedDbName;
        private string selectedDbUser, selectedDbPass;

        public Mainw()
        {
            InitializeComponent();

            string maindbpath = Application.StartupPath + "/db/main.db3";

            string emp_rif = "";

            string connection_str = "";

            if (!System.IO.File.Exists(maindbpath))
            {
                try
                {
                    if (!System.IO.Directory.Exists(Application.StartupPath + "/db"))
                    {
                        System.IO.DirectoryInfo di = System.IO.Directory.CreateDirectory(Application.StartupPath + "/db");
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Falla al crear el directorio de la Base de Datos.", "Error", MessageBoxButtons.OK);
                    Environment.Exit(-1);
                }

                PrimerUso pu = new PrimerUso();

                if(pu.ShowDialog() == DialogResult.OK)
                {
                    mainw_Conn = new SQLiteConnection("Data Source=" + maindbpath + "; Version=3;");

                    mainw_Conn.Open();

                    using (SQLiteCommand cmd = new SQLiteCommand())
                    {
                        cmd.Connection = mainw_Conn;
                        cmd.CommandText = "create table if not exists m_base " +
                        "(b_id varchar(10) primary key, " +
                        "b_nombre varchar(120) not null, b_tipobd varchar(16) not null, " +
                        "b_conn_str varchar(512) not null, b_usuario varchar(16), b_pass varchar(16))";

                        cmd.ExecuteNonQuery();

                        using (SQLiteTransaction trs = mainw_Conn.BeginTransaction())
                        {
                            cmd.CommandText = "insert into m_base values ('" + 
                                pu.Rif.ToString() + "', '" + pu.Nombre.ToString() + "'";

                            // Establece una base de datos SQLite local para su conexion.

                            if (pu.IsLocal)  
                            {
                                cmd.CommandText += ", '" + "sqlite" + "'";

                                selectedDbPath = Application.StartupPath + "/db/" + pu.Rif.ToString() + ".db3";

                                connection_str = "Data Source=" + selectedDbPath + "; Version=3;";

                                pu.DbUser = "null";
                            }
                            // Conecta a una de las bases de datos seleccionada (TO-DO)
                            else
                            {
                                cmd.CommandText += ", '" + pu.Bd_Type + "'";

                                if (pu.Bd_Type == "mysql")
                                {
                                    connection_str = "Server = " + pu.HostDir + "; " +
                                        "Database = " + "ce_maindb" + "; " +
                                        "UID = " + pu.DbUser + "; " + "Password = " + pu.DbUser + ";";
                                }

                                if (pu.Bd_Type == "postgresql")
                                {
                                    connection_str = String.Format("Server={0}; Port{1};" +
                                        "User Id={2}; Password={3}; Database={4};", pu.HostDir,
                                        "5432", pu.DbUser, pu.DbUser, "ce_maindb");
                                }
                            }

                            cmd.CommandText += ", '" + connection_str + "', '" +
                                pu.DbUser + "', '" + pu.DbUser + "')";

                            cmd.ExecuteNonQuery();

                            trs.Commit();
                        }
                    }

                    mainw_Conn.Close();
                }
                else
                {
                    MessageBox.Show("Debe completar los campos para empezar a usar el programa.", "Error", MessageBoxButtons.OK);
                    Environment.Exit(-1);
                }
            }

            mainw_lb_status.Text = "Abriendo Base de Datos";

            mainw_Conn = new SQLiteConnection("Data Source=" + maindbpath + "; Version=3;");

            OpenDatabase(mainw_Conn);

            mainw_lb_status.Text = "Abriendo Bases de Datos";

            using (SQLiteCommand cmd = new SQLiteCommand())
            {
                cmd.Connection = mainw_Conn;
                cmd.CommandText = "Select * from m_base";

                dr = cmd.ExecuteReader();
                
                while (dr.Read())
                {   
                    emp_rif = dr.GetString(0);
                    mainw_lb_client.Text = dr.GetString(0) + " - " + dr.GetString(1);
                    selectedDbType = dr.GetString(2);
                    connection_str = dr.GetString(3);
                    selectedDbUser = dr.GetString(4);
                    selectedDbPass = dr.GetString(5);
                }
            }

            if (selectedDbType == "sqlite")
            {
                mainw_db = new SQLiteConnection(connection_str);

                OpenDatabase(mainw_db);

                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    cmd.Connection = mainw_db;

                    // Crear la tabla de clientes

                    cmd.CommandText = "create table if not exists e_cliente " +
                    "(c_id varchar(10) primary key, " +
                    "c_nombre varchar(120) not null, c_entradas_adquiridas int not null, " +
                    "c_entradas_disponibles int not null, c_telefono varchar(11) not null," +
                    "c_correo varchar(120) not null)";

                    cmd.ExecuteNonQuery();

                    // Crear la tabla de transacciones

                    cmd.CommandText = "create table if not exists e_transaccion " +
                        "(t_id integer primary key autoincrement, " +
                        "t_entradas_adquiridas int not null, c_id varchar(10) not null," +
                        "foreign key(c_id) references e_cliente(c_id) on delete cascade on update cascade)";

                    cmd.ExecuteNonQuery();
                }
            }

            if (selectedDbType == "mysql")
            {
                //MySqlConnection connection = new MySqlConnection(connection_str);
            }

            if (selectedDbType == "postgresql")
            {
                //NpgsqlConnection connection = new NpgsqlConnection(connection_str)
            }

            CloseDatabase(mainw_Conn);

            mainw_pb_progressb.Hide();

            mainw_lb_status.Text = "Listo";
        }

        private void mainw_btn_add_Click(object sender, EventArgs e)
        {
            AgregarCliente ac = new AgregarCliente(mainw_db);

            ac.Show(this);
        }

        private void OpenDatabase(SQLiteConnection db)
        {
            db.Open();
        }

        private void CloseDatabase(SQLiteConnection db)
        {
            db.Close();
        }
    }
}
