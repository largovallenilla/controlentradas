﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Control_de_Entradas
{
    public partial class AgregarCliente : Form
    {
        private SQLiteConnection dbConn;

        public AgregarCliente()
        {
            InitializeComponent();
        }

        public AgregarCliente(SQLiteConnection maindbConn)
        {
            InitializeComponent();

            dbConn = maindbConn;

            ColocarPista("nombre");
            ColocarPista("id");
            ColocarPista("telefono");
            ColocarPista("correo");
            ColocarPista("entradas");
        }

        // Funcion extra: Agregar Entradas a un cliente

        private void SumarEntradas(string c_id, string entradas)
        {
            using (SQLiteCommand cmd = new SQLiteCommand())
            {
                cmd.Connection = dbConn;

                cmd.CommandText = "insert into e_transaccion (t_entradas_adquiridas, c_id)" +
                    " values(" + entradas + ", '" + c_id + "')";

                cmd.ExecuteNonQuery();

                cmd.CommandText = "update e_cliente set c_entradas_adquiridas = c_entradas_adquiridas + " + entradas + "," +
                    " c_entradas_disponibles = c_entradas_disponibles + " + entradas + " where c_id is '" + c_id + "'";

                cmd.ExecuteNonQuery();
            }
        }

        // Funciones de la clase

        private void btn_cancelar_Click(object sender, EventArgs e)
        {
            DialogResult resultado = MessageBox.Show("¿Desea cancelar esta acción?", "Cancelar", MessageBoxButtons.YesNo);

            if (resultado == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            if (DatosValidos())
            {
                DialogResult resultado = MessageBox.Show("¿Desea imprimir una tarjeta con los datos del nuevo cliente?", "Nuevo Cliente", MessageBoxButtons.YesNo);

                if (resultado == DialogResult.Yes)
                {
                    VerTarjeta vt = new VerTarjeta(tb_nombre.Text, cb_id.Text + tb_id.Text);

                    vt.ShowDialog(this);
                }

                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    cmd.Connection = dbConn;

                    cmd.CommandText = "insert into e_cliente" +
                        " values('" + cb_id.Text + tb_id.Text + "'," +
                        "'" + tb_nombre.Text + "', " + "0" + ", " + "0" + "," +
                        "'" + tb_telefono.Text + "', '" + tb_correo.Text + "')";

                    cmd.ExecuteNonQuery();
                }

                SumarEntradas(cb_id.Text + tb_id.Text, tb_entradas.Text);
            }
        }

        private bool CheckNombre()
        {
            bool flag = true;

            if (tb_nombre.Text == "" || tb_nombre.Text == "NOMBRE DEL CLIENTE")
            {
                flag = false;
                lb_nombre.ForeColor = Color.Red;
            }

            return flag;
        }

        private bool CheckId()
        {
            bool flag = true;

            if (tb_id.Text == "" ||  tb_id.Text == "123456789" || cb_id.SelectedIndex == 0)
            {
                flag = false;
                lb_rif.ForeColor = Color.Red;
            }

            return flag;
        }

        private bool CheckTelefono()
        {
            bool flag = true;

            string pattern = "0[24][1-9]{2}-[0-9]{7}";
            Regex r = new Regex(pattern);
            Match m = r.Match(tb_telefono.Text);

            string numero = string.Empty;

            while (m.Success)
            {
                numero += m.Groups[0].Value;
                m = m.NextMatch();
            }

            tb_telefono.Text = numero;

            if (tb_telefono.Text == "")
            {
                flag = false;
                ColocarPista("telefono");
                lb_telefono.ForeColor = Color.Red;
            }

            return flag;
        }

        private bool CheckCorreo()
        {
            bool flag = true;

            string pattern =
                @"^([0-9a-z]([\+\-_\.][0-9a-z]+)*)+" +
                @"@(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,5})$";
            Regex r = new Regex(pattern);
            Match m = r.Match(tb_correo.Text);

            string correo = string.Empty;

            while (m.Success)
            {
                correo += m.Groups[0].Value;
                m = m.NextMatch();
            }

            tb_correo.Text = correo;

            if (tb_correo.Text == "")
            {
                flag = false;
                ColocarPista("correo");
                lb_correo.ForeColor = Color.Red;
            }

            return flag;
        }

        private bool CheckEntradas()
        {
            bool flag = true;

            if (tb_entradas.Text == "" || tb_entradas.Text == "1 - 1000000")
            {
                flag = false;
                lb_entradas.ForeColor = Color.Red;
            }

            return flag;
        }

        private bool DatosValidos()
        {
            return (CheckNombre() && CheckId() && CheckTelefono() && CheckCorreo() && CheckEntradas());
        }

        private void tb_nombre_TextChanged(object sender, EventArgs e)
        {
            string pattern = @"[A-Z]?[A-Z\.\-\' ]{0,}";
            Regex r = new Regex(pattern);
            Match m = r.Match(tb_nombre.Text);

            string nombre = string.Empty;

            while (m.Success)
            {
                nombre += m.Groups[0].Value;
                m = m.NextMatch();
            }

            tb_nombre.Text = nombre;

            tb_nombre.SelectionStart = tb_nombre.Text.Length;

            if (lb_nombre.ForeColor == Color.Red)
            {
                lb_nombre.ForeColor = Color.Black;
            }
        }

        private void tb_id_TextChanged(object sender, EventArgs e)
        {
            string pattern = @"[1-9]?[1-9]{0,8}";
            Regex r = new Regex(pattern);
            Match m = r.Match(tb_id.Text);

            string id = string.Empty;

            while (m.Success)
            {
                id += m.Groups[0].Value;
                m = m.NextMatch();
            }

            tb_id.Text = id;

            tb_id.SelectionStart = tb_id.Text.Length;

            if (lb_rif.ForeColor == Color.Red)
            {
                lb_rif.ForeColor = Color.Black;
            }
        }

        private void tb_telefono_TextChanged(object sender, EventArgs e)
        {
            if (tb_telefono.Text.Length == 5)
            {
                if (tb_telefono.Text[4] != '-')
                    tb_telefono.Text = tb_telefono.Text.Insert(4, "-");
                else
                    tb_telefono.Text = tb_telefono.Text.Remove(4);
            }

            string pattern = "0?[24]?[1-9]{0,2}-?[0-9]{0,7}";
            Regex r = new Regex(pattern);
            Match m = r.Match(tb_telefono.Text);

            string numero = string.Empty;

            while (m.Success)
            {
                numero += m.Groups[0].Value;
                m = m.NextMatch();
            }

            tb_telefono.Text = numero;

            tb_telefono.SelectionStart = tb_telefono.Text.Length;

            if (lb_telefono.ForeColor == Color.Red)
            {
                lb_telefono.ForeColor = Color.Black;
            }
        }

        private void tb_correo_TextChanged(object sender, EventArgs e)
        {
            if (lb_correo.ForeColor == Color.Red)
            {
                lb_correo.ForeColor = Color.Black;
            }
        }

        private void tb_entradas_TextChanged(object sender, EventArgs e)
        {
            string pattern = @"[1-9]?[0-9]{0,6}";
            Regex r = new Regex(pattern);
            Match m = r.Match(tb_entradas.Text);

            string entradas = string.Empty;

            while (m.Success)
            {
                entradas += m.Groups[0].Value;
                m = m.NextMatch();
            }

            tb_entradas.Text = entradas;

            tb_entradas.SelectionStart = tb_entradas.Text.Length;

            if (lb_entradas.ForeColor == Color.Red)
            {
                lb_entradas.ForeColor = Color.Black;
            }
        }

        private void ColocarPista(string cod)
        {
            if (cod == "nombre")
            {
                tb_nombre.Text = "NOMBRE DEL CLIENTE";
                tb_nombre.ForeColor = Color.LightGray;
            }

            if (cod == "id")
            {
                tb_id.Text = "123456789";
                tb_id.ForeColor = Color.LightGray;
            }

            if (cod == "telefono")
            {
                tb_telefono.Text = "0416-1234567";
                tb_telefono.ForeColor = Color.LightGray;
            }

            if (cod == "correo")
            {
                tb_correo.Text = "ejemplo@correo.com";
                tb_correo.ForeColor = Color.LightGray;
            }

            if(cod == "entradas")
            {
                tb_entradas.Text = "1000000";
                tb_entradas.ForeColor = Color.LightGray;
            }
        }

        // Lo que pasa cuando obtiene el foco

        private void tb_nombre_GetFocus(object sender, EventArgs e)
        {
            if (tb_nombre.Text == "NOMBRE DEL CLIENTE")
            {
                tb_nombre.Text = "";
                tb_nombre.ForeColor = Color.Black;
            }
        }

        private void tb_id_GetFocus(object sender, EventArgs e)
        {
            if (tb_id.Text == "123456789")
            {
                tb_id.Text = "";
                tb_id.ForeColor = Color.Black;
            }
        }

        private void tb_telefono_GetFocus(object sender, EventArgs e)
        {
            if (tb_telefono.Text == "0416-1234567")
            {
                tb_telefono.Text = "";
                tb_telefono.ForeColor = Color.Black;
            }
        }

        private void tb_correo_GetFocus(object sender, EventArgs e)
        {
            if (tb_correo.Text == "ejemplo@correo.com")
            {
                tb_correo.Text = "";
                tb_correo.ForeColor = Color.Black;
            }
        }

        private void tb_entradas_GetFocus(object sender, EventArgs e)
        {
            if (tb_entradas.Text == "1000000")
            {
                tb_entradas.Text = "";
                tb_entradas.ForeColor = Color.Black;
            }
        }

        // Lo que pasa cuando se pierde el foco

        private void tb_nombre_LostFocus(object sender, EventArgs e)
        {
            if (tb_nombre.Text == "")
                ColocarPista("nombre");
        }

        private void tb_id_LostFocus(object sender, EventArgs e)
        {
            if (tb_id.Text == "")
                ColocarPista("id");
        }

        private void tb_telefono_LostFocus(object sender, EventArgs e)
        {
            if (tb_telefono.Text == "")
                ColocarPista("telefono");
        }

        private void tb_correo_LostFocus(object sender, EventArgs e)
        {
            if (tb_correo.Text == "")
                ColocarPista("correo");
        }

        private void tb_entradas_LostFocus(object sender, EventArgs e)
        {
            if (tb_entradas.Text == "")
                ColocarPista("entradas");
        }
    }
}
