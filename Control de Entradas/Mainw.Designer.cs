﻿namespace Control_de_Entradas
{
    partial class Mainw
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.mainw_lb_client = new System.Windows.Forms.Label();
            this.mainw_pb_progressb = new System.Windows.Forms.ProgressBar();
            this.mainw_lb_version = new System.Windows.Forms.Label();
            this.mainw_lb_status = new System.Windows.Forms.Label();
            this.mainw_btn_options = new System.Windows.Forms.Button();
            this.mainw_btn_stats = new System.Windows.Forms.Button();
            this.mainw_btn_add = new System.Windows.Forms.Button();
            this.mainw_btn_seek = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(25, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(16, 16, 8, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(279, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sistema de Control de Entradas";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mainw_lb_client
            // 
            this.mainw_lb_client.AutoSize = true;
            this.mainw_lb_client.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.mainw_lb_client.Location = new System.Drawing.Point(26, 50);
            this.mainw_lb_client.Margin = new System.Windows.Forms.Padding(16, 4, 0, 16);
            this.mainw_lb_client.Name = "mainw_lb_client";
            this.mainw_lb_client.Size = new System.Drawing.Size(43, 19);
            this.mainw_lb_client.TabIndex = 1;
            this.mainw_lb_client.Text = "-null-";
            this.mainw_lb_client.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mainw_pb_progressb
            // 
            this.mainw_pb_progressb.Location = new System.Drawing.Point(175, 237);
            this.mainw_pb_progressb.Margin = new System.Windows.Forms.Padding(4, 8, 4, 0);
            this.mainw_pb_progressb.Name = "mainw_pb_progressb";
            this.mainw_pb_progressb.Size = new System.Drawing.Size(302, 23);
            this.mainw_pb_progressb.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.mainw_pb_progressb.TabIndex = 8;
            // 
            // mainw_lb_version
            // 
            this.mainw_lb_version.ForeColor = System.Drawing.SystemColors.GrayText;
            this.mainw_lb_version.Location = new System.Drawing.Point(485, 237);
            this.mainw_lb_version.Margin = new System.Windows.Forms.Padding(4, 8, 8, 0);
            this.mainw_lb_version.Name = "mainw_lb_version";
            this.mainw_lb_version.Size = new System.Drawing.Size(100, 23);
            this.mainw_lb_version.TabIndex = 9;
            this.mainw_lb_version.Text = "Versión: 0.1 a";
            this.mainw_lb_version.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // mainw_lb_status
            // 
            this.mainw_lb_status.ForeColor = System.Drawing.SystemColors.GrayText;
            this.mainw_lb_status.Location = new System.Drawing.Point(17, 237);
            this.mainw_lb_status.Margin = new System.Windows.Forms.Padding(8, 8, 4, 0);
            this.mainw_lb_status.Name = "mainw_lb_status";
            this.mainw_lb_status.Size = new System.Drawing.Size(150, 23);
            this.mainw_lb_status.TabIndex = 10;
            this.mainw_lb_status.Text = "Listo";
            this.mainw_lb_status.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mainw_btn_options
            // 
            this.mainw_btn_options.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(97)))), ((int)(((byte)(97)))));
            this.mainw_btn_options.BackgroundImage = global::Control_de_Entradas.Properties.Resources.ic_settings_white_24dp_2x;
            this.mainw_btn_options.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.mainw_btn_options.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(97)))), ((int)(((byte)(97)))));
            this.mainw_btn_options.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.mainw_btn_options.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(66)))), ((int)(((byte)(66)))));
            this.mainw_btn_options.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mainw_btn_options.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.mainw_btn_options.ForeColor = System.Drawing.Color.White;
            this.mainw_btn_options.Location = new System.Drawing.Point(457, 93);
            this.mainw_btn_options.Margin = new System.Windows.Forms.Padding(16, 8, 16, 16);
            this.mainw_btn_options.Name = "mainw_btn_options";
            this.mainw_btn_options.Padding = new System.Windows.Forms.Padding(4);
            this.mainw_btn_options.Size = new System.Drawing.Size(120, 120);
            this.mainw_btn_options.TabIndex = 5;
            this.mainw_btn_options.Text = "&Opciones";
            this.mainw_btn_options.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.mainw_btn_options.UseVisualStyleBackColor = false;
            // 
            // mainw_btn_stats
            // 
            this.mainw_btn_stats.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.mainw_btn_stats.BackgroundImage = global::Control_de_Entradas.Properties.Resources.ic_show_chart_white_24dp_2x;
            this.mainw_btn_stats.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.mainw_btn_stats.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.mainw_btn_stats.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(94)))), ((int)(((byte)(32)))));
            this.mainw_btn_stats.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(125)))), ((int)(((byte)(50)))));
            this.mainw_btn_stats.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mainw_btn_stats.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.mainw_btn_stats.ForeColor = System.Drawing.Color.White;
            this.mainw_btn_stats.Location = new System.Drawing.Point(313, 93);
            this.mainw_btn_stats.Margin = new System.Windows.Forms.Padding(16, 8, 8, 16);
            this.mainw_btn_stats.Name = "mainw_btn_stats";
            this.mainw_btn_stats.Padding = new System.Windows.Forms.Padding(4);
            this.mainw_btn_stats.Size = new System.Drawing.Size(120, 120);
            this.mainw_btn_stats.TabIndex = 4;
            this.mainw_btn_stats.Text = "&Estadísticas";
            this.mainw_btn_stats.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.mainw_btn_stats.UseVisualStyleBackColor = false;
            // 
            // mainw_btn_add
            // 
            this.mainw_btn_add.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(121)))), ((int)(((byte)(107)))));
            this.mainw_btn_add.BackgroundImage = global::Control_de_Entradas.Properties.Resources.ic_person_add_white_24dp_2x;
            this.mainw_btn_add.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.mainw_btn_add.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(121)))), ((int)(((byte)(107)))));
            this.mainw_btn_add.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(77)))), ((int)(((byte)(64)))));
            this.mainw_btn_add.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(105)))), ((int)(((byte)(92)))));
            this.mainw_btn_add.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mainw_btn_add.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.mainw_btn_add.ForeColor = System.Drawing.Color.White;
            this.mainw_btn_add.Location = new System.Drawing.Point(169, 93);
            this.mainw_btn_add.Margin = new System.Windows.Forms.Padding(16, 8, 8, 16);
            this.mainw_btn_add.Name = "mainw_btn_add";
            this.mainw_btn_add.Padding = new System.Windows.Forms.Padding(4);
            this.mainw_btn_add.Size = new System.Drawing.Size(120, 120);
            this.mainw_btn_add.TabIndex = 3;
            this.mainw_btn_add.Text = "&Agregar Cliente";
            this.mainw_btn_add.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.mainw_btn_add.UseVisualStyleBackColor = false;
            this.mainw_btn_add.Click += new System.EventHandler(this.mainw_btn_add_Click);
            // 
            // mainw_btn_seek
            // 
            this.mainw_btn_seek.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(63)))), ((int)(((byte)(159)))));
            this.mainw_btn_seek.BackgroundImage = global::Control_de_Entradas.Properties.Resources.ic_search_white_24dp_2x;
            this.mainw_btn_seek.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.mainw_btn_seek.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(63)))), ((int)(((byte)(159)))));
            this.mainw_btn_seek.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(35)))), ((int)(((byte)(126)))));
            this.mainw_btn_seek.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(53)))), ((int)(((byte)(147)))));
            this.mainw_btn_seek.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.mainw_btn_seek.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.mainw_btn_seek.ForeColor = System.Drawing.Color.White;
            this.mainw_btn_seek.Location = new System.Drawing.Point(25, 93);
            this.mainw_btn_seek.Margin = new System.Windows.Forms.Padding(16, 8, 8, 16);
            this.mainw_btn_seek.Name = "mainw_btn_seek";
            this.mainw_btn_seek.Padding = new System.Windows.Forms.Padding(4);
            this.mainw_btn_seek.Size = new System.Drawing.Size(120, 120);
            this.mainw_btn_seek.TabIndex = 2;
            this.mainw_btn_seek.Text = "Ver &Clientes";
            this.mainw_btn_seek.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.mainw_btn_seek.UseVisualStyleBackColor = false;
            // 
            // Mainw
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(602, 261);
            this.Controls.Add(this.mainw_lb_status);
            this.Controls.Add(this.mainw_lb_version);
            this.Controls.Add(this.mainw_pb_progressb);
            this.Controls.Add(this.mainw_btn_options);
            this.Controls.Add(this.mainw_btn_stats);
            this.Controls.Add(this.mainw_btn_add);
            this.Controls.Add(this.mainw_btn_seek);
            this.Controls.Add(this.mainw_lb_client);
            this.Controls.Add(this.label1);
            this.Name = "Mainw";
            this.Text = "Control de Entradas";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label mainw_lb_client;
        private System.Windows.Forms.Button mainw_btn_seek;
        private System.Windows.Forms.Button mainw_btn_add;
        private System.Windows.Forms.Button mainw_btn_stats;
        private System.Windows.Forms.Button mainw_btn_options;
        private System.Windows.Forms.ProgressBar mainw_pb_progressb;
        private System.Windows.Forms.Label mainw_lb_version;
        private System.Windows.Forms.Label mainw_lb_status;
    }
}

