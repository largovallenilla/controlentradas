﻿namespace Control_de_Entradas
{
    partial class PrimerUso
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pu_lb_main = new System.Windows.Forms.Label();
            this.pu_tb_nombre = new System.Windows.Forms.TextBox();
            this.pu_lb_nombre = new System.Windows.Forms.Label();
            this.pu_lb_rif = new System.Windows.Forms.Label();
            this.pu_cb_rif = new System.Windows.Forms.ComboBox();
            this.pu_tb_rif = new System.Windows.Forms.TextBox();
            this.pu_btn_ok = new System.Windows.Forms.Button();
            this.gb_data = new System.Windows.Forms.GroupBox();
            this.gb_dbconn = new System.Windows.Forms.GroupBox();
            this.tb_dbuser = new System.Windows.Forms.TextBox();
            this.lb_dbuser = new System.Windows.Forms.Label();
            this.lb_host = new System.Windows.Forms.Label();
            this.tb_ipdir = new System.Windows.Forms.TextBox();
            this.pu_lb_dbsec = new System.Windows.Forms.Label();
            this.cb_dbtype = new System.Windows.Forms.ComboBox();
            this.rb_remotedb = new System.Windows.Forms.RadioButton();
            this.lb_dbtype = new System.Windows.Forms.Label();
            this.rb_localdb = new System.Windows.Forms.RadioButton();
            this.gb_data.SuspendLayout();
            this.gb_dbconn.SuspendLayout();
            this.SuspendLayout();
            // 
            // pu_lb_main
            // 
            this.pu_lb_main.AutoSize = true;
            this.pu_lb_main.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.pu_lb_main.Location = new System.Drawing.Point(16, 29);
            this.pu_lb_main.Margin = new System.Windows.Forms.Padding(16, 16, 8, 8);
            this.pu_lb_main.Name = "pu_lb_main";
            this.pu_lb_main.Size = new System.Drawing.Size(287, 19);
            this.pu_lb_main.TabIndex = 0;
            this.pu_lb_main.Text = "Por favor, introduzca los datos de su empresa";
            // 
            // pu_tb_nombre
            // 
            this.pu_tb_nombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pu_tb_nombre.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.pu_tb_nombre.Location = new System.Drawing.Point(86, 60);
            this.pu_tb_nombre.Margin = new System.Windows.Forms.Padding(4, 8, 16, 4);
            this.pu_tb_nombre.MaxLength = 120;
            this.pu_tb_nombre.Name = "pu_tb_nombre";
            this.pu_tb_nombre.Size = new System.Drawing.Size(256, 23);
            this.pu_tb_nombre.TabIndex = 1;
            this.pu_tb_nombre.TextChanged += new System.EventHandler(this.pu_tb_nombre_TextChange);
            // 
            // pu_lb_nombre
            // 
            this.pu_lb_nombre.AutoSize = true;
            this.pu_lb_nombre.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.pu_lb_nombre.Location = new System.Drawing.Point(16, 60);
            this.pu_lb_nombre.Margin = new System.Windows.Forms.Padding(16, 8, 4, 4);
            this.pu_lb_nombre.Name = "pu_lb_nombre";
            this.pu_lb_nombre.Size = new System.Drawing.Size(62, 19);
            this.pu_lb_nombre.TabIndex = 2;
            this.pu_lb_nombre.Text = "Nombre:";
            // 
            // pu_lb_rif
            // 
            this.pu_lb_rif.AutoSize = true;
            this.pu_lb_rif.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.pu_lb_rif.Location = new System.Drawing.Point(16, 95);
            this.pu_lb_rif.Margin = new System.Windows.Forms.Padding(16, 8, 4, 4);
            this.pu_lb_rif.Name = "pu_lb_rif";
            this.pu_lb_rif.Size = new System.Drawing.Size(31, 19);
            this.pu_lb_rif.TabIndex = 3;
            this.pu_lb_rif.Text = "RIF:";
            // 
            // pu_cb_rif
            // 
            this.pu_cb_rif.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pu_cb_rif.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.pu_cb_rif.FormattingEnabled = true;
            this.pu_cb_rif.Items.AddRange(new object[] {
            "-",
            "V",
            "E",
            "P",
            "G",
            "J",
            "C"});
            this.pu_cb_rif.Location = new System.Drawing.Point(86, 94);
            this.pu_cb_rif.Margin = new System.Windows.Forms.Padding(4, 4, 4, 16);
            this.pu_cb_rif.Name = "pu_cb_rif";
            this.pu_cb_rif.Size = new System.Drawing.Size(44, 23);
            this.pu_cb_rif.TabIndex = 4;
            this.pu_cb_rif.SelectedIndexChanged += new System.EventHandler(this.pu_tb_rif_TextChange);
            this.pu_cb_rif.TextUpdate += new System.EventHandler(this.pu_tb_rif_TextChange);
            // 
            // pu_tb_rif
            // 
            this.pu_tb_rif.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pu_tb_rif.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.pu_tb_rif.Location = new System.Drawing.Point(138, 94);
            this.pu_tb_rif.Margin = new System.Windows.Forms.Padding(4, 8, 16, 8);
            this.pu_tb_rif.MaxLength = 9;
            this.pu_tb_rif.Name = "pu_tb_rif";
            this.pu_tb_rif.Size = new System.Drawing.Size(204, 23);
            this.pu_tb_rif.TabIndex = 5;
            this.pu_tb_rif.TextChanged += new System.EventHandler(this.pu_tb_rif_TextChange);
            // 
            // pu_btn_ok
            // 
            this.pu_btn_ok.Location = new System.Drawing.Point(144, 364);
            this.pu_btn_ok.Margin = new System.Windows.Forms.Padding(16, 8, 16, 8);
            this.pu_btn_ok.Name = "pu_btn_ok";
            this.pu_btn_ok.Size = new System.Drawing.Size(96, 32);
            this.pu_btn_ok.TabIndex = 6;
            this.pu_btn_ok.Text = "Guardar Datos";
            this.pu_btn_ok.UseVisualStyleBackColor = true;
            this.pu_btn_ok.Click += new System.EventHandler(this.pu_btn_ok_Click);
            // 
            // gb_data
            // 
            this.gb_data.Controls.Add(this.pu_lb_main);
            this.gb_data.Controls.Add(this.pu_lb_nombre);
            this.gb_data.Controls.Add(this.pu_tb_rif);
            this.gb_data.Controls.Add(this.pu_tb_nombre);
            this.gb_data.Controls.Add(this.pu_cb_rif);
            this.gb_data.Controls.Add(this.pu_lb_rif);
            this.gb_data.Location = new System.Drawing.Point(13, 13);
            this.gb_data.Margin = new System.Windows.Forms.Padding(4, 4, 4, 8);
            this.gb_data.Name = "gb_data";
            this.gb_data.Padding = new System.Windows.Forms.Padding(0);
            this.gb_data.Size = new System.Drawing.Size(358, 133);
            this.gb_data.TabIndex = 7;
            this.gb_data.TabStop = false;
            this.gb_data.Text = "Datos de la Empresa";
            // 
            // gb_dbconn
            // 
            this.gb_dbconn.Controls.Add(this.tb_dbuser);
            this.gb_dbconn.Controls.Add(this.lb_dbuser);
            this.gb_dbconn.Controls.Add(this.lb_host);
            this.gb_dbconn.Controls.Add(this.tb_ipdir);
            this.gb_dbconn.Controls.Add(this.pu_lb_dbsec);
            this.gb_dbconn.Controls.Add(this.cb_dbtype);
            this.gb_dbconn.Controls.Add(this.rb_remotedb);
            this.gb_dbconn.Controls.Add(this.lb_dbtype);
            this.gb_dbconn.Controls.Add(this.rb_localdb);
            this.gb_dbconn.Location = new System.Drawing.Point(13, 162);
            this.gb_dbconn.Margin = new System.Windows.Forms.Padding(4, 8, 4, 8);
            this.gb_dbconn.Name = "gb_dbconn";
            this.gb_dbconn.Padding = new System.Windows.Forms.Padding(0);
            this.gb_dbconn.Size = new System.Drawing.Size(358, 186);
            this.gb_dbconn.TabIndex = 8;
            this.gb_dbconn.TabStop = false;
            this.gb_dbconn.Text = "Conexión a la Base de Datos";
            // 
            // tb_dbuser
            // 
            this.tb_dbuser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_dbuser.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tb_dbuser.Location = new System.Drawing.Point(146, 155);
            this.tb_dbuser.Margin = new System.Windows.Forms.Padding(4, 8, 16, 8);
            this.tb_dbuser.MaxLength = 9;
            this.tb_dbuser.Name = "tb_dbuser";
            this.tb_dbuser.Size = new System.Drawing.Size(196, 23);
            this.tb_dbuser.TabIndex = 7;
            this.tb_dbuser.TextChanged += new System.EventHandler(this.tb_dbuser_TextChanged);
            // 
            // lb_dbuser
            // 
            this.lb_dbuser.AutoSize = true;
            this.lb_dbuser.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lb_dbuser.Location = new System.Drawing.Point(16, 155);
            this.lb_dbuser.Margin = new System.Windows.Forms.Padding(16, 8, 4, 4);
            this.lb_dbuser.Name = "lb_dbuser";
            this.lb_dbuser.Size = new System.Drawing.Size(59, 19);
            this.lb_dbuser.TabIndex = 6;
            this.lb_dbuser.Text = "Usuario:";
            // 
            // lb_host
            // 
            this.lb_host.AutoSize = true;
            this.lb_host.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lb_host.Location = new System.Drawing.Point(16, 93);
            this.lb_host.Margin = new System.Windows.Forms.Padding(16, 8, 4, 4);
            this.lb_host.Name = "lb_host";
            this.lb_host.Size = new System.Drawing.Size(122, 19);
            this.lb_host.TabIndex = 8;
            this.lb_host.Text = "Dirección Servidor:";
            // 
            // tb_ipdir
            // 
            this.tb_ipdir.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_ipdir.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tb_ipdir.Location = new System.Drawing.Point(146, 93);
            this.tb_ipdir.Margin = new System.Windows.Forms.Padding(4, 8, 16, 4);
            this.tb_ipdir.MaxLength = 120;
            this.tb_ipdir.Name = "tb_ipdir";
            this.tb_ipdir.Size = new System.Drawing.Size(196, 23);
            this.tb_ipdir.TabIndex = 7;
            this.tb_ipdir.TextChanged += new System.EventHandler(this.tb_ipdir_TextChanged);
            // 
            // pu_lb_dbsec
            // 
            this.pu_lb_dbsec.AutoSize = true;
            this.pu_lb_dbsec.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.pu_lb_dbsec.Location = new System.Drawing.Point(16, 29);
            this.pu_lb_dbsec.Margin = new System.Windows.Forms.Padding(16, 16, 8, 8);
            this.pu_lb_dbsec.Name = "pu_lb_dbsec";
            this.pu_lb_dbsec.Size = new System.Drawing.Size(323, 19);
            this.pu_lb_dbsec.TabIndex = 7;
            this.pu_lb_dbsec.Text = "Especifique el tipo de conexión que usará el sistema";
            // 
            // cb_dbtype
            // 
            this.cb_dbtype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_dbtype.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.cb_dbtype.FormattingEnabled = true;
            this.cb_dbtype.Items.AddRange(new object[] {
            "(Seleccionar)",
            "MySQL",
            "PostgreSQL"});
            this.cb_dbtype.Location = new System.Drawing.Point(146, 124);
            this.cb_dbtype.Margin = new System.Windows.Forms.Padding(4, 4, 4, 16);
            this.cb_dbtype.Name = "cb_dbtype";
            this.cb_dbtype.Size = new System.Drawing.Size(196, 23);
            this.cb_dbtype.TabIndex = 10;
            this.cb_dbtype.SelectedIndexChanged += new System.EventHandler(this.cb_dbtype_SelectedIndexChanged);
            // 
            // rb_remotedb
            // 
            this.rb_remotedb.AutoSize = true;
            this.rb_remotedb.Location = new System.Drawing.Point(209, 60);
            this.rb_remotedb.Margin = new System.Windows.Forms.Padding(8, 4, 16, 8);
            this.rb_remotedb.Name = "rb_remotedb";
            this.rb_remotedb.Size = new System.Drawing.Size(133, 17);
            this.rb_remotedb.TabIndex = 1;
            this.rb_remotedb.Text = "Conexión remota (Red)";
            this.rb_remotedb.UseVisualStyleBackColor = true;
            // 
            // lb_dbtype
            // 
            this.lb_dbtype.AutoSize = true;
            this.lb_dbtype.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lb_dbtype.Location = new System.Drawing.Point(16, 125);
            this.lb_dbtype.Margin = new System.Windows.Forms.Padding(16, 8, 4, 4);
            this.lb_dbtype.Name = "lb_dbtype";
            this.lb_dbtype.Size = new System.Drawing.Size(99, 19);
            this.lb_dbtype.TabIndex = 9;
            this.lb_dbtype.Text = "Base de Datos:";
            // 
            // rb_localdb
            // 
            this.rb_localdb.AutoSize = true;
            this.rb_localdb.Location = new System.Drawing.Point(12, 59);
            this.rb_localdb.Margin = new System.Windows.Forms.Padding(16, 4, 8, 8);
            this.rb_localdb.Name = "rb_localdb";
            this.rb_localdb.Size = new System.Drawing.Size(157, 17);
            this.rb_localdb.TabIndex = 0;
            this.rb_localdb.Text = "BD Local (Solo este equipo)";
            this.rb_localdb.UseVisualStyleBackColor = true;
            this.rb_localdb.CheckedChanged += new System.EventHandler(this.rb_localdb_CheckedChanged);
            // 
            // PrimerUso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 413);
            this.Controls.Add(this.gb_dbconn);
            this.Controls.Add(this.pu_btn_ok);
            this.Controls.Add(this.gb_data);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximumSize = new System.Drawing.Size(400, 452);
            this.MinimumSize = new System.Drawing.Size(400, 452);
            this.Name = "PrimerUso";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Bienvenido";
            this.gb_data.ResumeLayout(false);
            this.gb_data.PerformLayout();
            this.gb_dbconn.ResumeLayout(false);
            this.gb_dbconn.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label pu_lb_main;
        private System.Windows.Forms.TextBox pu_tb_nombre;
        private System.Windows.Forms.Label pu_lb_nombre;
        private System.Windows.Forms.Label pu_lb_rif;
        private System.Windows.Forms.ComboBox pu_cb_rif;
        private System.Windows.Forms.TextBox pu_tb_rif;
        private System.Windows.Forms.Button pu_btn_ok;
        private System.Windows.Forms.GroupBox gb_data;
        private System.Windows.Forms.GroupBox gb_dbconn;
        private System.Windows.Forms.TextBox tb_dbuser;
        private System.Windows.Forms.Label lb_dbuser;
        private System.Windows.Forms.Label lb_host;
        private System.Windows.Forms.TextBox tb_ipdir;
        private System.Windows.Forms.Label pu_lb_dbsec;
        private System.Windows.Forms.ComboBox cb_dbtype;
        private System.Windows.Forms.RadioButton rb_remotedb;
        private System.Windows.Forms.Label lb_dbtype;
        private System.Windows.Forms.RadioButton rb_localdb;
    }
}