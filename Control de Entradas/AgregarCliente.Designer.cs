﻿namespace Control_de_Entradas
{
    partial class AgregarCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pu_lb_main = new System.Windows.Forms.Label();
            this.lb_nombre = new System.Windows.Forms.Label();
            this.tb_nombre = new System.Windows.Forms.TextBox();
            this.tb_id = new System.Windows.Forms.TextBox();
            this.cb_id = new System.Windows.Forms.ComboBox();
            this.lb_rif = new System.Windows.Forms.Label();
            this.lb_entradas = new System.Windows.Forms.Label();
            this.btn_cancelar = new System.Windows.Forms.Button();
            this.btn_guardar = new System.Windows.Forms.Button();
            this.lb_correo = new System.Windows.Forms.Label();
            this.tb_correo = new System.Windows.Forms.TextBox();
            this.lb_telefono = new System.Windows.Forms.Label();
            this.tb_telefono = new System.Windows.Forms.TextBox();
            this.tb_entradas = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // pu_lb_main
            // 
            this.pu_lb_main.AutoSize = true;
            this.pu_lb_main.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.pu_lb_main.Location = new System.Drawing.Point(25, 25);
            this.pu_lb_main.Margin = new System.Windows.Forms.Padding(16, 16, 8, 8);
            this.pu_lb_main.Name = "pu_lb_main";
            this.pu_lb_main.Size = new System.Drawing.Size(243, 19);
            this.pu_lb_main.TabIndex = 9;
            this.pu_lb_main.Text = "Introduzca los datos del nuevo cliente:";
            // 
            // lb_nombre
            // 
            this.lb_nombre.AutoSize = true;
            this.lb_nombre.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lb_nombre.Location = new System.Drawing.Point(25, 60);
            this.lb_nombre.Margin = new System.Windows.Forms.Padding(16, 8, 4, 4);
            this.lb_nombre.Name = "lb_nombre";
            this.lb_nombre.Size = new System.Drawing.Size(62, 19);
            this.lb_nombre.TabIndex = 10;
            this.lb_nombre.Text = "Nombre:";
            // 
            // tb_nombre
            // 
            this.tb_nombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_nombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tb_nombre.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tb_nombre.Location = new System.Drawing.Point(95, 60);
            this.tb_nombre.Margin = new System.Windows.Forms.Padding(4, 8, 16, 4);
            this.tb_nombre.MaxLength = 120;
            this.tb_nombre.Name = "tb_nombre";
            this.tb_nombre.Size = new System.Drawing.Size(264, 23);
            this.tb_nombre.TabIndex = 1;
            this.tb_nombre.TextChanged += new System.EventHandler(this.tb_nombre_TextChanged);
            this.tb_nombre.Enter += new System.EventHandler(this.tb_nombre_GetFocus);
            this.tb_nombre.Leave += new System.EventHandler(this.tb_nombre_LostFocus);
            // 
            // tb_id
            // 
            this.tb_id.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_id.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tb_id.Location = new System.Drawing.Point(167, 90);
            this.tb_id.Margin = new System.Windows.Forms.Padding(4, 8, 16, 8);
            this.tb_id.MaxLength = 9;
            this.tb_id.Name = "tb_id";
            this.tb_id.Size = new System.Drawing.Size(192, 23);
            this.tb_id.TabIndex = 3;
            this.tb_id.TextChanged += new System.EventHandler(this.tb_id_TextChanged);
            this.tb_id.Enter += new System.EventHandler(this.tb_id_GetFocus);
            this.tb_id.Leave += new System.EventHandler(this.tb_id_LostFocus);
            // 
            // cb_id
            // 
            this.cb_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_id.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.cb_id.FormattingEnabled = true;
            this.cb_id.Items.AddRange(new object[] {
            "-",
            "V",
            "E",
            "P",
            "G",
            "J",
            "C"});
            this.cb_id.Location = new System.Drawing.Point(115, 89);
            this.cb_id.Margin = new System.Windows.Forms.Padding(4, 4, 4, 16);
            this.cb_id.Name = "cb_id";
            this.cb_id.Size = new System.Drawing.Size(44, 23);
            this.cb_id.TabIndex = 2;
            // 
            // lb_rif
            // 
            this.lb_rif.AutoSize = true;
            this.lb_rif.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lb_rif.Location = new System.Drawing.Point(25, 91);
            this.lb_rif.Margin = new System.Windows.Forms.Padding(16, 8, 4, 4);
            this.lb_rif.Name = "lb_rif";
            this.lb_rif.Size = new System.Drawing.Size(49, 19);
            this.lb_rif.TabIndex = 11;
            this.lb_rif.Text = "RIF/CI:";
            // 
            // lb_entradas
            // 
            this.lb_entradas.AutoSize = true;
            this.lb_entradas.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lb_entradas.Location = new System.Drawing.Point(25, 184);
            this.lb_entradas.Margin = new System.Windows.Forms.Padding(16, 8, 4, 4);
            this.lb_entradas.Name = "lb_entradas";
            this.lb_entradas.Size = new System.Drawing.Size(134, 19);
            this.lb_entradas.TabIndex = 12;
            this.lb_entradas.Text = "Entradas Adquiridas:";
            // 
            // btn_cancelar
            // 
            this.btn_cancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_cancelar.Location = new System.Drawing.Point(151, 223);
            this.btn_cancelar.Margin = new System.Windows.Forms.Padding(16, 8, 8, 8);
            this.btn_cancelar.Name = "btn_cancelar";
            this.btn_cancelar.Size = new System.Drawing.Size(96, 32);
            this.btn_cancelar.TabIndex = 7;
            this.btn_cancelar.Text = "Cancelar";
            this.btn_cancelar.UseVisualStyleBackColor = true;
            this.btn_cancelar.Click += new System.EventHandler(this.btn_cancelar_Click);
            // 
            // btn_guardar
            // 
            this.btn_guardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_guardar.Location = new System.Drawing.Point(263, 223);
            this.btn_guardar.Margin = new System.Windows.Forms.Padding(8, 8, 16, 8);
            this.btn_guardar.Name = "btn_guardar";
            this.btn_guardar.Size = new System.Drawing.Size(96, 32);
            this.btn_guardar.TabIndex = 8;
            this.btn_guardar.Text = "Guardar Datos";
            this.btn_guardar.UseVisualStyleBackColor = true;
            this.btn_guardar.Click += new System.EventHandler(this.btn_guardar_Click);
            // 
            // lb_correo
            // 
            this.lb_correo.AutoSize = true;
            this.lb_correo.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lb_correo.Location = new System.Drawing.Point(25, 153);
            this.lb_correo.Margin = new System.Windows.Forms.Padding(16, 8, 4, 4);
            this.lb_correo.Name = "lb_correo";
            this.lb_correo.Size = new System.Drawing.Size(124, 19);
            this.lb_correo.TabIndex = 14;
            this.lb_correo.Text = "Correo Electrónico:";
            // 
            // tb_correo
            // 
            this.tb_correo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_correo.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tb_correo.Location = new System.Drawing.Point(167, 153);
            this.tb_correo.Margin = new System.Windows.Forms.Padding(4, 8, 16, 4);
            this.tb_correo.MaxLength = 120;
            this.tb_correo.Name = "tb_correo";
            this.tb_correo.Size = new System.Drawing.Size(192, 23);
            this.tb_correo.TabIndex = 5;
            this.tb_correo.TextChanged += new System.EventHandler(this.tb_correo_TextChanged);
            this.tb_correo.Enter += new System.EventHandler(this.tb_correo_GetFocus);
            this.tb_correo.Leave += new System.EventHandler(this.tb_correo_LostFocus);
            // 
            // lb_telefono
            // 
            this.lb_telefono.AutoSize = true;
            this.lb_telefono.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.lb_telefono.Location = new System.Drawing.Point(25, 122);
            this.lb_telefono.Margin = new System.Windows.Forms.Padding(16, 8, 4, 4);
            this.lb_telefono.Name = "lb_telefono";
            this.lb_telefono.Size = new System.Drawing.Size(90, 19);
            this.lb_telefono.TabIndex = 16;
            this.lb_telefono.Text = "Nro Teléfono:";
            // 
            // tb_telefono
            // 
            this.tb_telefono.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_telefono.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tb_telefono.Location = new System.Drawing.Point(167, 122);
            this.tb_telefono.Margin = new System.Windows.Forms.Padding(4, 8, 16, 4);
            this.tb_telefono.MaxLength = 12;
            this.tb_telefono.Name = "tb_telefono";
            this.tb_telefono.Size = new System.Drawing.Size(192, 23);
            this.tb_telefono.TabIndex = 4;
            this.tb_telefono.TextChanged += new System.EventHandler(this.tb_telefono_TextChanged);
            this.tb_telefono.Enter += new System.EventHandler(this.tb_telefono_GetFocus);
            this.tb_telefono.Leave += new System.EventHandler(this.tb_telefono_LostFocus);
            // 
            // tb_entradas
            // 
            this.tb_entradas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_entradas.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tb_entradas.Location = new System.Drawing.Point(167, 184);
            this.tb_entradas.Margin = new System.Windows.Forms.Padding(4, 8, 16, 8);
            this.tb_entradas.MaxLength = 6;
            this.tb_entradas.Name = "tb_entradas";
            this.tb_entradas.Size = new System.Drawing.Size(192, 23);
            this.tb_entradas.TabIndex = 6;
            this.tb_entradas.TextChanged += new System.EventHandler(this.tb_entradas_TextChanged);
            this.tb_entradas.Enter += new System.EventHandler(this.tb_entradas_GetFocus);
            this.tb_entradas.Leave += new System.EventHandler(this.tb_entradas_LostFocus);
            // 
            // AgregarCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 272);
            this.Controls.Add(this.tb_entradas);
            this.Controls.Add(this.lb_telefono);
            this.Controls.Add(this.tb_telefono);
            this.Controls.Add(this.lb_correo);
            this.Controls.Add(this.tb_correo);
            this.Controls.Add(this.btn_guardar);
            this.Controls.Add(this.btn_cancelar);
            this.Controls.Add(this.lb_entradas);
            this.Controls.Add(this.tb_id);
            this.Controls.Add(this.cb_id);
            this.Controls.Add(this.lb_rif);
            this.Controls.Add(this.lb_nombre);
            this.Controls.Add(this.tb_nombre);
            this.Controls.Add(this.pu_lb_main);
            this.MaximumSize = new System.Drawing.Size(400, 311);
            this.MinimumSize = new System.Drawing.Size(400, 311);
            this.Name = "AgregarCliente";
            this.Text = "Agregar Cliente";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label pu_lb_main;
        private System.Windows.Forms.Label lb_nombre;
        private System.Windows.Forms.TextBox tb_nombre;
        private System.Windows.Forms.TextBox tb_id;
        private System.Windows.Forms.ComboBox cb_id;
        private System.Windows.Forms.Label lb_rif;
        private System.Windows.Forms.Label lb_entradas;
        private System.Windows.Forms.Button btn_cancelar;
        private System.Windows.Forms.Button btn_guardar;
        private System.Windows.Forms.Label lb_correo;
        private System.Windows.Forms.TextBox tb_correo;
        private System.Windows.Forms.Label lb_telefono;
        private System.Windows.Forms.TextBox tb_telefono;
        private System.Windows.Forms.TextBox tb_entradas;
    }
}