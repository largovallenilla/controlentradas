﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZXing;
using ZXing.QrCode;
using ZXing.QrCode.Internal;
using ZXing.Common;

namespace Control_de_Entradas
{
    public partial class VerTarjeta : Form
    {
        private QRCode code;

        public VerTarjeta()
        {
            InitializeComponent();
        }

        public VerTarjeta(string nombre, string id)
        {
            InitializeComponent();

            lb_clientName.Text = nombre;
            lb_clientID.Text = id;

            BarcodeWriter bw = new BarcodeWriter();
            EncodingOptions eop = new EncodingOptions()
            { Width = pb_qr.Width, Height = pb_qr.Height, Margin = 0 };

            bw.Options = eop;
            bw.Format = ZXing.BarcodeFormat.QR_CODE;

            pb_qr.Image = bw.Write(id);
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {

        }
    }
}
