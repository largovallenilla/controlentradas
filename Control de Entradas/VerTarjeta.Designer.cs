﻿namespace Control_de_Entradas
{
    partial class VerTarjeta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TBL_Tarjeta = new System.Windows.Forms.TableLayoutPanel();
            this.pb_qr = new System.Windows.Forms.PictureBox();
            this.lb_nombreEmpresa = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lb_clientName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lb_clientID = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_guardar = new System.Windows.Forms.Button();
            this.TBL_Tarjeta.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_qr)).BeginInit();
            this.SuspendLayout();
            // 
            // TBL_Tarjeta
            // 
            this.TBL_Tarjeta.BackColor = System.Drawing.Color.White;
            this.TBL_Tarjeta.ColumnCount = 3;
            this.TBL_Tarjeta.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.TBL_Tarjeta.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.TBL_Tarjeta.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.TBL_Tarjeta.Controls.Add(this.pb_qr, 2, 0);
            this.TBL_Tarjeta.Controls.Add(this.lb_nombreEmpresa, 0, 1);
            this.TBL_Tarjeta.Controls.Add(this.label4, 0, 2);
            this.TBL_Tarjeta.Controls.Add(this.label2, 0, 3);
            this.TBL_Tarjeta.Controls.Add(this.lb_clientName, 1, 3);
            this.TBL_Tarjeta.Controls.Add(this.label3, 0, 5);
            this.TBL_Tarjeta.Controls.Add(this.lb_clientID, 1, 5);
            this.TBL_Tarjeta.Location = new System.Drawing.Point(9, 38);
            this.TBL_Tarjeta.Margin = new System.Windows.Forms.Padding(0, 0, 0, 8);
            this.TBL_Tarjeta.MaximumSize = new System.Drawing.Size(320, 130);
            this.TBL_Tarjeta.MinimumSize = new System.Drawing.Size(320, 130);
            this.TBL_Tarjeta.Name = "TBL_Tarjeta";
            this.TBL_Tarjeta.RowCount = 8;
            this.TBL_Tarjeta.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.TBL_Tarjeta.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.TBL_Tarjeta.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.TBL_Tarjeta.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.TBL_Tarjeta.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.TBL_Tarjeta.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.TBL_Tarjeta.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.TBL_Tarjeta.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.TBL_Tarjeta.Size = new System.Drawing.Size(320, 130);
            this.TBL_Tarjeta.TabIndex = 0;
            // 
            // pb_qr
            // 
            this.pb_qr.Location = new System.Drawing.Point(195, 5);
            this.pb_qr.Margin = new System.Windows.Forms.Padding(5);
            this.pb_qr.Name = "pb_qr";
            this.TBL_Tarjeta.SetRowSpan(this.pb_qr, 8);
            this.pb_qr.Size = new System.Drawing.Size(120, 120);
            this.pb_qr.TabIndex = 0;
            this.pb_qr.TabStop = false;
            // 
            // lb_nombreEmpresa
            // 
            this.lb_nombreEmpresa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_nombreEmpresa.AutoSize = true;
            this.TBL_Tarjeta.SetColumnSpan(this.lb_nombreEmpresa, 2);
            this.lb_nombreEmpresa.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_nombreEmpresa.Location = new System.Drawing.Point(3, 17);
            this.lb_nombreEmpresa.Name = "lb_nombreEmpresa";
            this.lb_nombreEmpresa.Size = new System.Drawing.Size(184, 13);
            this.lb_nombreEmpresa.TabIndex = 1;
            this.lb_nombreEmpresa.Text = "OPERADORA AQUA MUNDO, C.A.";
            this.lb_nombreEmpresa.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.TBL_Tarjeta.SetColumnSpan(this.label4, 2);
            this.label4.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.label4.Location = new System.Drawing.Point(3, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(184, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "TARJETA DE CLIENTE";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(3, 57);
            this.label2.Name = "label2";
            this.TBL_Tarjeta.SetRowSpan(this.label2, 2);
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nombre";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_clientName
            // 
            this.lb_clientName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_clientName.AutoSize = true;
            this.lb_clientName.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.lb_clientName.Location = new System.Drawing.Point(63, 57);
            this.lb_clientName.Name = "lb_clientName";
            this.TBL_Tarjeta.SetRowSpan(this.lb_clientName, 2);
            this.lb_clientName.Size = new System.Drawing.Size(124, 13);
            this.lb_clientName.TabIndex = 6;
            this.lb_clientName.Text = "<NOMBRE>";
            this.lb_clientName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(3, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "CI / RIF";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_clientID
            // 
            this.lb_clientID.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lb_clientID.AutoSize = true;
            this.lb_clientID.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.lb_clientID.Location = new System.Drawing.Point(63, 81);
            this.lb_clientID.Name = "lb_clientID";
            this.lb_clientID.Size = new System.Drawing.Size(124, 13);
            this.lb_clientID.TabIndex = 7;
            this.lb_clientID.Text = "<ID>";
            this.lb_clientID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.label1.Location = new System.Drawing.Point(17, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Vista Previa";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_guardar
            // 
            this.btn_guardar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_guardar.Location = new System.Drawing.Point(121, 184);
            this.btn_guardar.Margin = new System.Windows.Forms.Padding(8, 8, 16, 8);
            this.btn_guardar.Name = "btn_guardar";
            this.btn_guardar.Size = new System.Drawing.Size(96, 32);
            this.btn_guardar.TabIndex = 9;
            this.btn_guardar.Text = "Imprimir";
            this.btn_guardar.UseVisualStyleBackColor = true;
            this.btn_guardar.Click += new System.EventHandler(this.btn_guardar_Click);
            // 
            // VerTarjeta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(338, 233);
            this.Controls.Add(this.btn_guardar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TBL_Tarjeta);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(354, 272);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(354, 272);
            this.Name = "VerTarjeta";
            this.Text = "Tarjeta de Cliente";
            this.TBL_Tarjeta.ResumeLayout(false);
            this.TBL_Tarjeta.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_qr)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel TBL_Tarjeta;
        private System.Windows.Forms.PictureBox pb_qr;
        private System.Windows.Forms.Label lb_nombreEmpresa;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lb_clientName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lb_clientID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_guardar;
    }
}