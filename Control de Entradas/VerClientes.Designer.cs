﻿namespace Control_de_Entradas
{
    partial class VerClientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_clientes = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.disponibles = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.gb_Busqueda = new System.Windows.Forms.GroupBox();
            this.btn_qr = new System.Windows.Forms.Button();
            this.btn_consultar = new System.Windows.Forms.Button();
            this.tb_nombre = new System.Windows.Forms.TextBox();
            this.tb_id = new System.Windows.Forms.TextBox();
            this.cb_id = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_clientes)).BeginInit();
            this.gb_Busqueda.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv_clientes
            // 
            this.dgv_clientes.AllowUserToAddRows = false;
            this.dgv_clientes.AllowUserToDeleteRows = false;
            this.dgv_clientes.AllowUserToResizeColumns = false;
            this.dgv_clientes.AllowUserToResizeRows = false;
            this.dgv_clientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_clientes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.nombre,
            this.disponibles});
            this.dgv_clientes.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgv_clientes.Location = new System.Drawing.Point(276, 9);
            this.dgv_clientes.Margin = new System.Windows.Forms.Padding(0);
            this.dgv_clientes.MinimumSize = new System.Drawing.Size(440, 423);
            this.dgv_clientes.Name = "dgv_clientes";
            this.dgv_clientes.ReadOnly = true;
            this.dgv_clientes.RowHeadersWidth = 20;
            this.dgv_clientes.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_clientes.Size = new System.Drawing.Size(496, 423);
            this.dgv_clientes.TabIndex = 0;
            // 
            // id
            // 
            this.id.Frozen = true;
            this.id.HeaderText = "CI/RIF";
            this.id.MinimumWidth = 100;
            this.id.Name = "id";
            this.id.ReadOnly = true;
            // 
            // nombre
            // 
            this.nombre.Frozen = true;
            this.nombre.HeaderText = "Nombre del Cliente";
            this.nombre.MinimumWidth = 281;
            this.nombre.Name = "nombre";
            this.nombre.ReadOnly = true;
            this.nombre.Width = 281;
            // 
            // disponibles
            // 
            this.disponibles.Frozen = true;
            this.disponibles.HeaderText = "Entradas Disponibles";
            this.disponibles.MinimumWidth = 75;
            this.disponibles.Name = "disponibles";
            this.disponibles.ReadOnly = true;
            this.disponibles.Width = 75;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.label1.Location = new System.Drawing.Point(17, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Acciones Rápidas";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gb_Busqueda
            // 
            this.gb_Busqueda.Controls.Add(this.btn_qr);
            this.gb_Busqueda.Controls.Add(this.btn_consultar);
            this.gb_Busqueda.Controls.Add(this.tb_nombre);
            this.gb_Busqueda.Controls.Add(this.tb_id);
            this.gb_Busqueda.Controls.Add(this.cb_id);
            this.gb_Busqueda.Location = new System.Drawing.Point(9, 42);
            this.gb_Busqueda.Margin = new System.Windows.Forms.Padding(0, 4, 8, 8);
            this.gb_Busqueda.Name = "gb_Busqueda";
            this.gb_Busqueda.Size = new System.Drawing.Size(259, 138);
            this.gb_Busqueda.TabIndex = 10;
            this.gb_Busqueda.TabStop = false;
            this.gb_Busqueda.Text = "Búsqueda";
            // 
            // btn_qr
            // 
            this.btn_qr.Location = new System.Drawing.Point(152, 20);
            this.btn_qr.Margin = new System.Windows.Forms.Padding(8);
            this.btn_qr.Name = "btn_qr";
            this.btn_qr.Size = new System.Drawing.Size(96, 107);
            this.btn_qr.TabIndex = 10;
            this.btn_qr.Text = "Escanear QR";
            this.btn_qr.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.btn_qr.UseVisualStyleBackColor = true;
            // 
            // btn_consultar
            // 
            this.btn_consultar.Location = new System.Drawing.Point(7, 95);
            this.btn_consultar.Margin = new System.Windows.Forms.Padding(8);
            this.btn_consultar.Name = "btn_consultar";
            this.btn_consultar.Size = new System.Drawing.Size(129, 32);
            this.btn_consultar.TabIndex = 9;
            this.btn_consultar.Text = "Consultar";
            this.btn_consultar.UseVisualStyleBackColor = true;
            // 
            // tb_nombre
            // 
            this.tb_nombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_nombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tb_nombre.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tb_nombre.Location = new System.Drawing.Point(7, 60);
            this.tb_nombre.Margin = new System.Windows.Forms.Padding(4, 8, 8, 4);
            this.tb_nombre.MaxLength = 120;
            this.tb_nombre.Name = "tb_nombre";
            this.tb_nombre.Size = new System.Drawing.Size(129, 23);
            this.tb_nombre.TabIndex = 6;
            // 
            // tb_id
            // 
            this.tb_id.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_id.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.tb_id.Location = new System.Drawing.Point(59, 21);
            this.tb_id.Margin = new System.Windows.Forms.Padding(4, 8, 8, 8);
            this.tb_id.MaxLength = 9;
            this.tb_id.Name = "tb_id";
            this.tb_id.Size = new System.Drawing.Size(77, 23);
            this.tb_id.TabIndex = 5;
            // 
            // cb_id
            // 
            this.cb_id.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_id.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.cb_id.FormattingEnabled = true;
            this.cb_id.Items.AddRange(new object[] {
            "-",
            "V",
            "E",
            "P",
            "G",
            "J",
            "C"});
            this.cb_id.Location = new System.Drawing.Point(7, 20);
            this.cb_id.Margin = new System.Windows.Forms.Padding(4, 4, 4, 16);
            this.cb_id.Name = "cb_id";
            this.cb_id.Size = new System.Drawing.Size(44, 23);
            this.cb_id.TabIndex = 4;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Location = new System.Drawing.Point(9, 192);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(0, 4, 8, 8);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(259, 240);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Acciones";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(137, 133);
            this.button4.Margin = new System.Windows.Forms.Padding(8);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(96, 96);
            this.button4.TabIndex = 14;
            this.button4.Text = "Eliminar";
            this.button4.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(25, 133);
            this.button3.Margin = new System.Windows.Forms.Padding(8);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(96, 96);
            this.button3.TabIndex = 13;
            this.button3.Text = "Editar";
            this.button3.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(137, 24);
            this.button2.Margin = new System.Windows.Forms.Padding(8);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(96, 96);
            this.button2.TabIndex = 12;
            this.button2.Text = "Ver Datos";
            this.button2.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(25, 24);
            this.button1.Margin = new System.Windows.Forms.Padding(8);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 96);
            this.button1.TabIndex = 11;
            this.button1.Text = "Agregar";
            this.button1.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // VerClientes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 441);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.gb_Busqueda);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgv_clientes);
            this.MaximumSize = new System.Drawing.Size(800, 480);
            this.MinimumSize = new System.Drawing.Size(800, 480);
            this.Name = "VerClientes";
            this.Text = "Clientes";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_clientes)).EndInit();
            this.gb_Busqueda.ResumeLayout(false);
            this.gb_Busqueda.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_clientes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gb_Busqueda;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tb_id;
        private System.Windows.Forms.ComboBox cb_id;
        private System.Windows.Forms.TextBox tb_nombre;
        private System.Windows.Forms.Button btn_qr;
        private System.Windows.Forms.Button btn_consultar;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn disponibles;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
    }
}