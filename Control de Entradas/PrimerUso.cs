﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Control_de_Entradas
{
    public partial class PrimerUso : Form
    {
        public string Nombre { get; set; }
        public string Rif { get; set; }
        public string Bd_Type { get; set; }
        public string HostDir { get; set; }
        public string DbUser { get; set; }
        public bool IsLocal { get; set; }

        private bool[] checklist;

        private string db_str, db_usr;

        public PrimerUso()
        {
            InitializeComponent();

            pu_cb_rif.SelectedIndex = 0;
            cb_dbtype.SelectedIndex = 0;

            checklist = new bool[6];

            for (int i = 0; i < checklist.Length; i++)
            {
                checklist[i] = false;
            }
        }

        private void pu_btn_ok_Click(object sender, EventArgs e)
        {

            if (pu_tb_nombre.Text != "")
            {
                Nombre = pu_tb_nombre.Text;
            }
            else
            {
                checklist[0] = true;
                checklist[1] = true;
            }

            if (pu_cb_rif.SelectedIndex != 0 && pu_tb_rif.Text != "")
            {
                Rif = pu_cb_rif.Text + pu_tb_rif.Text;
            }
            else
            {
                checklist[0] = true;
                checklist[2] = true;
            }

            if (rb_remotedb.Checked)
            {
                if (tb_ipdir.Text != "")
                {
                    db_str = tb_ipdir.Text;
                }
                else
                {
                    checklist[0] = true;
                    checklist[3] = true;
                }

                if (tb_dbuser.Text != "")
                {
                    db_usr = tb_dbuser.Text;
                }
                else
                {
                    checklist[0] = true;
                    checklist[5] = true;
                }

                if (cb_dbtype.SelectedIndex != 0)
                {
                    switch (cb_dbtype.SelectedIndex)
                    {
                        case 1: //MySQL
                            Bd_Type = "mysql";
                            break;

                        case 2: //PostgreSQL
                            Bd_Type = "postgresql";
                            break;

                        /*case 3: //MongoDB
                            Bd_Type = "mongodb";
                            break;*/
                    }

                    HostDir = tb_ipdir.Text;
                    DbUser = tb_dbuser.Text;
                }
                else
                {
                    checklist[0] = true;
                    checklist[4] = true;
                }

                IsLocal = false;
            }
            else
            {
                IsLocal = true;
            }

            if (!checklist[0])
            {
                DialogResult = DialogResult.OK;
                this.Close();
            }
            else
                MostrarError(checklist);
        }

        private void pu_tb_rif_TextChange(object sender, EventArgs e)
        {
            pu_lb_main.ForeColor = Color.Black;
            pu_lb_rif.ForeColor = Color.Black;
        }

        private void pu_tb_nombre_TextChange(object sender, EventArgs e)
        {
            pu_lb_main.ForeColor = Color.Black;
            pu_lb_nombre.ForeColor = Color.Black;
        }

        private void MostrarError(bool[] checklist)
        {
            if (checklist[1])
            {
                pu_lb_nombre.ForeColor = Color.Red;
            }

            if (checklist[2])
            {
                pu_lb_rif.ForeColor = Color.Red;
            }

            if (checklist[3])
            {
                lb_host.ForeColor = Color.Red;
            }

            if (checklist[4])
            {
                lb_dbtype.ForeColor = Color.Red;
            }

            if (checklist[5])
            {
                lb_dbuser.ForeColor = Color.Red;
            }

            pu_lb_main.ForeColor = Color.Red;
            pu_lb_dbsec.ForeColor = Color.Red;
        }

        private void tb_ipdir_TextChanged(object sender, EventArgs e)
        {
            pu_lb_dbsec.ForeColor = Color.Black;
            lb_host.ForeColor = Color.Black;
        }

        private void cb_dbtype_SelectedIndexChanged(object sender, EventArgs e)
        {
            pu_lb_dbsec.ForeColor = Color.Black;
            lb_dbtype.ForeColor = Color.Black;
        }

        private void tb_dbuser_TextChanged(object sender, EventArgs e)
        {
            pu_lb_dbsec.ForeColor = Color.Black;
            lb_dbuser.ForeColor = Color.Black;
        }

        private void rb_localdb_CheckedChanged(object sender, EventArgs e)
        {
            if(rb_localdb.Checked)
            {
                tb_ipdir.Enabled = false;
                tb_dbuser.Enabled = false;
                cb_dbtype.Enabled = false;
            }
            else
            {
                tb_ipdir.Enabled = true;
                tb_dbuser.Enabled = true;
                cb_dbtype.Enabled = true;
            }
        }
    }
}
